import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RestrictedRoutingModule } from './restricted-routing.module';
import { DashboardComponent } from './views/dashboard/dashboard.component';
import { StockAnalysisComponent } from './views/stock-analysis/stock-analysis.component';
import { StoreModule } from '@ngrx/store';
import { reducers, effects } from './store';
import { EffectsModule } from '@ngrx/effects';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatCardModule } from '@angular/material/card';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatListModule } from '@angular/material/list';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HighchartsChartModule } from 'highcharts-angular';

import * as fromServices from './services';
import * as fromPipes from './pipes';
import { HttpClientModule } from '@angular/common/http';
import { SideBySideComponent } from './views/side-by-side/side-by-side.component';
import { ReactiveFormsModule } from '@angular/forms';
import { TickerSearchComponent } from './components/ticker-search/ticker-search.component';
import { TickerFilterPipe } from './pipes';
import { StockProfileComponent } from './components/stock-profile/stock-profile.component';
import { MarketCapPipe } from './pipes/market-cap.pipe';
import { StockReportComponent } from './components/stock-report/stock-report.component';
import { StockValuationComponent } from './components/stock-valuation/stock-valuation.component';
import { StockChartComponent } from './components/stock-chart/stock-chart.component';
import { ValuationPipePipe } from './pipes/valuation-pipe.pipe';
import { StockNewsComponent } from './components/stock-news/stock-news.component';
import { StockPerformanceComponent } from './components/stock-performance/stock-performance.component';
import { StockRatiosComponent } from './components/stock-ratios/stock-ratios.component';
import { FinancialChartComponent } from './components/financial-chart/financial-chart.component';

@NgModule({
  declarations: [
    DashboardComponent,
    StockAnalysisComponent,
    SideBySideComponent,
    TickerSearchComponent,
    TickerFilterPipe,
    StockProfileComponent,
    MarketCapPipe,
    StockReportComponent,
    StockValuationComponent,
    StockChartComponent,
    ValuationPipePipe,
    StockNewsComponent,
    StockPerformanceComponent,
    StockRatiosComponent,
    FinancialChartComponent,
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    HighchartsChartModule,
    RestrictedRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    MatAutocompleteModule,
    MatCardModule,
    MatProgressSpinnerModule,
    MatExpansionModule,
    MatListModule,
    StoreModule.forFeature('appRestricted', reducers),
    EffectsModule.forFeature(effects),
  ],
  providers: [...fromServices.services],
})
export class RestrictedModule {}
