import {
  Component,
  Input,
  OnInit,
  HostListener,
  ElementRef,
} from '@angular/core';
import * as Highcharts from 'highcharts';

@Component({
  selector: 'app-stock-chart',
  templateUrl: './stock-chart.component.html',
  styleUrls: ['./stock-chart.component.scss'],
})
export class StockChartComponent implements OnInit {
  constructor(private host: ElementRef) {}
  @Input() options: any;
  highcharts = Highcharts;

  // Might not need this
  @HostListener('window:resize', ['$event'])
  onResize(): void {
    const styles = getComputedStyle(this.host.nativeElement);

    const chart = this.options.chart || {};
    chart.width = +styles.width.slice(0, styles.width.length - 2).split('.')[0];
    this.options = {
      ...this.options,
      chart,
    };
  }

  ngOnInit(): void {}
}
