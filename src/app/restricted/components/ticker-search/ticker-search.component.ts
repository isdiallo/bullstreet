import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import * as fromRestrictedStore from '../../store';

import { Ticker } from '../../models/ticker.model';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-ticker-search',
  templateUrl: './ticker-search.component.html',
  styleUrls: ['./ticker-search.component.scss'],
})
export class TickerSearchComponent implements OnInit {
  constructor(
    private tickersStore: Store<fromRestrictedStore.AppRestrictedState>
  ) {}
  @Input() autoComplete = true; // Show an auto complete list of tickers
  @Input() debounceTime = 300; // debounce between each BE call, default 300
  @Output() selectedTicker = new EventEmitter<string>(); // Event with the selected ticker
  ticker = new FormControl('');
  tickers$: Observable<Ticker[]>;

  search(): void {
    this.selectedTicker.emit(this.ticker.value);
  }

  ngOnInit(): void {
    if (this.autoComplete) {
      this.ticker.valueChanges
        .pipe(debounceTime(this.debounceTime), distinctUntilChanged())
        .subscribe((ticker) =>
          this.tickersStore.dispatch(
            new fromRestrictedStore.SearchTickers(ticker)
          )
        );
    }

    this.tickers$ = this.tickersStore.select(
      fromRestrictedStore.getTickersList
    );
  }
}
