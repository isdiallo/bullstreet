import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { configurations } from '../../configurations';
import { BalanceSheetModel } from '../../models/balance-sheet.model';
import { CashFlowModel } from '../../models/cash-flow.model';
import { IncomeStatementModel } from '../../models/income-statement.model';
import { StockRatiosModel } from '../../models/stock-ratios.model';
import { TimeSeriesService } from '../../services';

@Component({
  selector: 'app-financial-chart',
  templateUrl: './financial-chart.component.html',
  styleUrls: ['./financial-chart.component.scss'],
})
export class FinancialChartComponent implements OnInit, OnChanges {
  constructor(private tsService: TimeSeriesService) {}

  @Input() data:
    | CashFlowModel[]
    | BalanceSheetModel[]
    | IncomeStatementModel[]
    | StockRatiosModel[];
  @Input() type: string;
  chartOptions: any;
  private configurations = configurations;

  computeChartOptions(
    ratios:
      | StockRatiosModel[]
      | BalanceSheetModel[]
      | IncomeStatementModel[]
      | CashFlowModel[],
    conf: any
  ): void {
    if (ratios && ratios.length > 0) {
      const timeSeries = this.tsService.computeSeries(ratios);
      const { date, finalLink, symbol, ...filteredSeries } = timeSeries;
      const series = [];
      const { fieldsName, name, nonVisible } = conf;

      Object.keys(filteredSeries).forEach((k) => {
        if (Object.keys(fieldsName).indexOf(k) >= 0) {
          series.push({
            name: fieldsName[k].name,
            data: filteredSeries[k],
            visible: nonVisible.indexOf(k) >= 0 ? false : true,
          });
        }
      });
      this.chartOptions = {
        title: {
          text: `<strong>${name}</strong>`,
        },
        yAxis: {
          title: {
            text: '',
          },
        },
        xAxis: {
          categories: date,
        },
        credits: {
          enabled: false,
        },
        series,
      };
    }
  }

  ngOnInit(): void {
    if (this.type && this.configurations[this.type]) {
      this.computeChartOptions(this.data, this.configurations[this.type]);
    }
  }
  ngOnChanges(): void {
    if (this.type && this.configurations[this.type]) {
      this.computeChartOptions(this.data, this.configurations[this.type]);
    }
  }
}
