import { Component, Input, OnInit } from '@angular/core';
import { BalanceSheetModel } from '../../models/balance-sheet.model';
import { CashFlowModel } from '../../models/cash-flow.model';
import { DcfModel } from '../../models/dcf.model';
import { IncomeStatementModel } from '../../models/income-statement.model';
import { NewsModel } from '../../models/news.model';
import { StockRatiosModel } from '../../models/stock-ratios.model';

@Component({
  selector: 'app-stock-report',
  templateUrl: './stock-report.component.html',
  styleUrls: ['./stock-report.component.scss'],
})
export class StockReportComponent implements OnInit {
  constructor() {}

  @Input() dcf: DcfModel;
  @Input() news: NewsModel[];
  @Input() cashFlow: CashFlowModel[];
  @Input() incomeStatement: IncomeStatementModel[];
  @Input() balanceSheet: BalanceSheetModel[];
  @Input() ratios: StockRatiosModel[];

  ngOnInit(): void {}
}
