import { Component, Input, OnInit } from '@angular/core';
import { StockProfile } from '../../models/profile.model';

@Component({
  selector: 'app-stock-profile',
  templateUrl: './stock-profile.component.html',
  styleUrls: ['./stock-profile.component.scss'],
})
export class StockProfileComponent implements OnInit {
  constructor() {}
  @Input() profile: StockProfile;

  ngOnInit(): void {}
}
