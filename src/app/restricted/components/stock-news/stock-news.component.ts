import { Component, Input, OnInit } from '@angular/core';
import { NewsModel } from '../../models/news.model';

@Component({
  selector: 'app-stock-news',
  templateUrl: './stock-news.component.html',
  styleUrls: ['./stock-news.component.scss'],
})
export class StockNewsComponent implements OnInit {
  constructor() {}
  @Input() news: NewsModel[];
  @Input() expanded = false;

  ngOnInit(): void {}
}
