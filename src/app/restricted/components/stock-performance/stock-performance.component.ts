import { Component, Input, OnInit } from '@angular/core';
import { BalanceSheetModel } from '../../models/balance-sheet.model';
import { CashFlowModel } from '../../models/cash-flow.model';
import { IncomeStatementModel } from '../../models/income-statement.model';

@Component({
  selector: 'app-stock-performance',
  templateUrl: './stock-performance.component.html',
  styleUrls: ['./stock-performance.component.scss'],
})
export class StockPerformanceComponent implements OnInit {
  constructor() {}
  @Input() cashFlow: CashFlowModel[];
  @Input() incomeStatement: IncomeStatementModel[];
  @Input() balanceSheet: BalanceSheetModel[];
  @Input() expanded = false;

  ngOnInit(): void {}
}
