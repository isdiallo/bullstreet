import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { DcfModel } from '../../models/dcf.model';
import { isNumber } from 'lodash-es';

@Component({
  selector: 'app-stock-valuation',
  templateUrl: './stock-valuation.component.html',
  styleUrls: ['./stock-valuation.component.scss'],
})
export class StockValuationComponent implements OnInit, OnChanges {
  constructor() {}
  @Input() dcf: DcfModel;
  @Input() expanded = false;
  options: any;
  ratio: number;
  validDcf: boolean;

  computeOptions(dcf: DcfModel): void {
    this.validDcf = dcf && isNumber(dcf.dcf) && dcf.dcf > 0;
    if (this.validDcf) {
      this.ratio = 1 - dcf['Stock Price'] / dcf.dcf;
      this.options = {
        chart: {
          type: 'column',
        },
        title: {
          text: `Intrinsic Value vs Current Stock Price as of ${dcf.date}`,
        },
        xAxis: {
          categories: [dcf.symbol.toUpperCase()],
        },
        credits: {
          enabled: false,
        },
        series: [
          {
            name: 'DCF',
            data: [dcf.dcf],
          },
          {
            name: 'Stock Price',
            data: [dcf['Stock Price']],
          },
        ],
      };
    }
  }
  ngOnInit(): void {
    this.computeOptions(this.dcf);
  }
  ngOnChanges(): void {
    this.computeOptions(this.dcf);
  }
}
