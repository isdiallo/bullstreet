import { Component, Input, OnInit } from '@angular/core';
import { StockRatiosModel } from '../../models/stock-ratios.model';

@Component({
  selector: 'app-stock-ratios',
  templateUrl: './stock-ratios.component.html',
  styleUrls: ['./stock-ratios.component.scss'],
})
export class StockRatiosComponent implements OnInit {
  constructor() {}
  @Input() ratios: StockRatiosModel[];

  ngOnInit(): void {}
}
