import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StockRatiosComponent } from './stock-ratios.component';

describe('StockRatiosComponent', () => {
  let component: StockRatiosComponent;
  let fixture: ComponentFixture<StockRatiosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StockRatiosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StockRatiosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
