export const balanceSheetConfiguration = {
  balanceSheet: {
    nonVisible: ['shortTermInvestments', 'cashAndShortTermInvestments'],
    name: 'Current and Previous Balance Sheet Data',
    fieldsName: {
      symbol: { name: 'Ticker', description: '' },
      date: { name: 'Date', description: '' },
      cashAndCashEquivalents: { name: 'Cash & Equivalents', description: '' },
      cashAndShortTermInvestments: {
        name: 'Cash & Short Term Invest.',
        description: '',
      },
      totalLiabilities: { name: 'Liabilities', description: '' },
      totalCurrentLiabilities: { name: 'Current Liabilities', description: '' },
      totalCurrentAssets: { name: 'Current Assets', description: '' },
      longTermDebt: { name: 'Long Term Debt', description: '' },
      totalAssets: { name: 'Total Assets', description: '' },
      finalLink: { name: 'Sec. Link', description: '' },
    },
  },
};
