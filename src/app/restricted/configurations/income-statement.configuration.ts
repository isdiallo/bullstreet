export const incomeStatementConfiguration = {
  incomeStatement: {
    nonVisible: ['eps', 'ebitda'],
    name: 'Current and Previous Income Statement Data',
    fieldsName: {
      symbol: { name: 'Ticker', description: '' },
      date: { name: 'Date', description: '' },
      netIncome: { name: 'Net Income', description: '' },
      revenue: { name: 'Revenue', description: '' },
      costOfRevenue: { name: 'Cost Of Rev.', description: '' },
      costAndExpenses: { name: 'Costs & Expenses', description: '' },
      eps: { name: 'EPS', description: '' },
      ebitda: { name: 'EBITDA', description: '' },
      finalLink: { name: 'Sec Link', description: '' },
    },
  },
};
