import { balanceSheetConfiguration } from './balance-sheet.configuration';
import { cashFlowConfiguration } from './cash-flow.configuration';
import { incomeStatementConfiguration } from './income-statement.configuration';
import { ratiosConfigurations } from './ratios.configuration';

export const configurations = {
  ...balanceSheetConfiguration,
  ...cashFlowConfiguration,
  ...incomeStatementConfiguration,
  ...ratiosConfigurations,
};
