export const cashFlowConfiguration = {
  cashFlow: {
    nonVisible: ['netChangeInCash', 'capitalExpenditure'],
    name: 'Current and Previous Cash Flow Data',
    fieldsName: {
      date: { name: 'Date', description: '' },
      symbol: { name: 'Ticker', description: '' },
      netIncome: { name: 'Net Income', description: '' },
      operatingCashFlow: { name: 'Operating Cash Flow', description: '' },
      netCashUsedProvidedByFinancingActivities: {
        name: 'Net cash in financing',
        description: '',
      },
      netChangeInCash: { name: 'Cash and equiv. increase', description: '' },
      capitalExpenditure: { name: 'Cap. Exp.', description: '' },
      freeCashFlow: { name: 'Free Cash Flow', description: '' },
      dividendsPaid: { name: 'Paid Div.', description: '' },
      finalLink: { name: 'Sec Link', description: '' },
    },
  },
};
