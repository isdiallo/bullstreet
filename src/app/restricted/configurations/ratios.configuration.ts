export const ratiosConfigurations = {
  debt: {
    nonVisible: ['longTermDebtToCapitalization'],
    name: 'Debt Ratios',
    fieldsName: {
      symbol: { name: 'Ticker', description: '' },
      date: { name: 'Date', description: '' },
      debtRatio: { name: 'Debt Ratio', description: '' },
      debtEquityRatio: { name: 'Debt to Equity', description: '' },
      longTermDebtToCapitalization: {
        name: 'Long Terme Debt to Cap.',
        description: '',
      },
      totalDebtToCapitalization: {
        name: 'Total Debt to Cap.',
        description: '',
      },
      cashFlowToDebtRatio: { name: 'Cash Flow to Debt', description: '' },
    },
  },
  price: {
    nonVisible: [
      'priceBookValueRatio',
      'priceToOperatingCashFlowsRatio',
      'priceToSalesRatio',
      'priceFairValue',
    ],
    name: 'Price Ratios',
    fieldsName: {
      symbol: { name: 'Ticker', description: '' },
      date: { name: 'Date', description: '' },
      priceEarningsRatio: { name: 'Price To Earnings', description: '' },
      priceBookValueRatio: { name: 'Price to Book Value', description: '' },
      priceToBookRatio: { name: 'Price to Book', description: '' },
      priceToSalesRatio: { name: 'Price to Sales', description: '' },
      priceToFreeCashFlowsRatio: {
        name: 'Price to Free Cash Flow',
        description: '',
      },
      priceToOperatingCashFlowsRatio: {
        name: 'Price to Op. Cash Flow',
        description: '',
      },
      priceFairValue: { name: 'Price Fair Value', description: '' },
    },
  },
  profit: {
    nonVisible: [],
    fieldsName: {
      symbol: { name: 'Ticker', description: '' },
      date: { name: 'Date', description: '' },
      grossProfitMargin: { name: 'Gross Profit Margin', description: '' },
      netProfitMargin: { name: 'Net Profit Margin', description: '' },
      returnOnAssets: { name: 'Return On Assets', description: '' },
      returnOnEquity: { name: 'Return On Equity', description: '' },
      ebitPerRevenue: { name: 'EBIT per Revenue', description: '' },
    },
    name: 'Profit Ratios',
  },
  basic: {
    nonVisible: [],
    fieldsName: {
      symbol: { name: 'Ticker', description: '' },
      date: { name: 'Date', description: '' },
      cashRatio: { name: 'Cash Ratio', description: '' },
      quickRatio: { name: 'Quick Ratio', description: '' },
      returnOnCapitalEmployed: { name: 'ROCE', description: '' },
      payoutRatio: { name: 'Payout Ratio', description: '' },
    },
    name: 'Basics Ratios',
  },
};
