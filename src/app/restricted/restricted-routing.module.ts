import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './views/dashboard/dashboard.component';
import { SideBySideComponent } from './views/side-by-side/side-by-side.component';
import { StockAnalysisComponent } from './views/stock-analysis/stock-analysis.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
  },
  {
    path: 'analysis',
    component: StockAnalysisComponent,
  },
  {
    path: 'side-by-side',
    component: SideBySideComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RestrictedRoutingModule {}
