import { BalanceSheetModel } from '../../models/balance-sheet.model';
import { CashFlowModel } from '../../models/cash-flow.model';
import { DcfModel } from '../../models/dcf.model';
import { IncomeStatementModel } from '../../models/income-statement.model';
import { NewsModel } from '../../models/news.model';
import { StockRatiosModel } from '../../models/stock-ratios.model';
import * as fromStockAnalysis from '../actions/stock-analysis.action';
export interface StockAnalysisState {
  dcfEntities: { [id: string]: DcfModel };
  newsEntities: { [id: string]: NewsModel[] };
  cashFlowEntities: { [id: string]: CashFlowModel[] };
  incomeStatementEntities: { [id: string]: IncomeStatementModel[] };
  balanceSheetEntities: { [id: string]: BalanceSheetModel[] };
  ratiosEntities: { [id: string]: StockRatiosModel[] };
  dcfLoading: boolean;
  dcfLoaded: boolean;
  newsLoading: boolean;
  newsLoaded: boolean;
  cashFlowLoading: boolean;
  cashFlowLoaded: boolean;
  incomeStatementLoading: boolean;
  incomeStatementLoaded: boolean;
  balanceSheetLoading: boolean;
  balanceSheetLoaded: boolean;
  ratiosLoaded: boolean;
  ratiosLoading: boolean;
}

export const initialState: StockAnalysisState = {
  dcfEntities: {},
  newsEntities: {},
  cashFlowEntities: {},
  incomeStatementEntities: {},
  balanceSheetEntities: {},
  ratiosEntities: {},
  dcfLoaded: false,
  dcfLoading: false,
  newsLoaded: false,
  newsLoading: false,
  balanceSheetLoaded: false,
  balanceSheetLoading: false,
  cashFlowLoaded: false,
  cashFlowLoading: false,
  incomeStatementLoaded: false,
  incomeStatementLoading: false,
  ratiosLoaded: false,
  ratiosLoading: false,
};

export function reducer(
  state = initialState,
  action: fromStockAnalysis.StockAnalysisAction
): StockAnalysisState {
  switch (action.type) {
    case fromStockAnalysis.LOAD_DCF: {
      return {
        ...state,
        dcfLoaded: false,
        dcfLoading: true,
      };
    }
    case fromStockAnalysis.LOAD_DCF_SUCCESS: {
      const dcf = action.payload[0];
      let dcfEntities = {};
      if (dcf) {
        dcfEntities = {
          ...state.dcfEntities,
          [dcf.symbol.toUpperCase()]: dcf,
        };
      } else {
        dcfEntities = {
          ...state.dcfEntities,
        };
      }
      return {
        ...state,
        dcfLoading: false,
        dcfLoaded: dcf ? true : false,
        dcfEntities,
      };
    }
    case fromStockAnalysis.LOAD_DCF_FAIL: {
      return {
        ...state,
        dcfLoading: false,
        dcfLoaded: false,
      };
    }
    case fromStockAnalysis.LOAD_NEWS: {
      return {
        ...state,
        newsLoaded: false,
        newsLoading: true,
      };
    }
    case fromStockAnalysis.LOAD_NEWS_SUCCESS: {
      const news = action.payload;
      const ticker = news[0];
      let newsEntities = {};
      if (ticker && ticker.symbol) {
        newsEntities = {
          ...state.newsEntities,
          [ticker.symbol.toUpperCase()]: news,
        };
      } else {
        newsEntities = {
          ...state.newsEntities,
        };
      }
      return {
        ...state,
        newsLoading: false,
        newsLoaded: true,
        newsEntities,
      };
    }
    case fromStockAnalysis.LOAD_NEWS_FAIL: {
      return {
        ...state,
        newsLoading: false,
        newsLoaded: false,
      };
    }
    case fromStockAnalysis.LOAD_CASH_FLOW: {
      return {
        ...state,
        cashFlowLoaded: false,
        cashFlowLoading: true,
      };
    }
    case fromStockAnalysis.LOAD_CASH_FLOW_SUCCESS: {
      const cfs: CashFlowModel[] = action.payload;
      let ticker = '';
      if (cfs.length > 0) {
        ticker = cfs[0].symbol;
      }
      let cashFlowEntities = {};
      if (ticker) {
        cashFlowEntities = {
          ...state.cashFlowEntities,
          [ticker.toUpperCase()]: cfs,
        };
      } else {
        cashFlowEntities = {
          ...state.cashFlowEntities,
        };
      }
      return {
        ...state,
        cashFlowEntities,
        cashFlowLoaded: true,
        cashFlowLoading: false,
      };
    }
    case fromStockAnalysis.LOAD_CASH_FLOW_FAIL: {
      return {
        ...state,
        cashFlowLoaded: false,
        cashFlowLoading: false,
      };
    }
    case fromStockAnalysis.LOAD_INCOME_STATEMENT: {
      return {
        ...state,
        incomeStatementLoaded: false,
        incomeStatementLoading: true,
      };
    }
    case fromStockAnalysis.LOAD_INCOME_STATEMENT_SUCCESS: {
      const iss: IncomeStatementModel[] = action.payload;
      let ticker = '';
      if (iss.length > 0) {
        ticker = iss[0].symbol;
      }
      let incomeStatementEntities = {};
      if (ticker) {
        incomeStatementEntities = {
          ...state.incomeStatementEntities,
          [ticker.toUpperCase()]: iss,
        };
      } else {
        incomeStatementEntities = {
          ...state.incomeStatementEntities,
        };
      }
      return {
        ...state,
        incomeStatementEntities,
        incomeStatementLoaded: true,
        incomeStatementLoading: false,
      };
    }
    case fromStockAnalysis.LOAD_INCOME_STATEMENT_FAIL: {
      return {
        ...state,
        incomeStatementLoading: false,
        incomeStatementLoaded: false,
      };
    }
    case fromStockAnalysis.LOAD_BALANCE_SHEET: {
      return {
        ...state,
        balanceSheetLoaded: false,
        balanceSheetLoading: true,
      };
    }
    case fromStockAnalysis.LOAD_BALANCE_SHEET_SUCCESS: {
      const bss: BalanceSheetModel[] = action.payload;
      let ticker = '';
      if (bss.length > 0) {
        ticker = bss[0].symbol;
      }
      let balanceSheetEntities = {};
      if (ticker) {
        balanceSheetEntities = {
          ...state.balanceSheetEntities,
          [ticker.toUpperCase()]: bss,
        };
      } else {
        balanceSheetEntities = {
          ...state.balanceSheetEntities,
        };
      }
      return {
        ...state,
        balanceSheetEntities,
        balanceSheetLoaded: true,
        balanceSheetLoading: false,
      };
    }
    case fromStockAnalysis.LOAD_BALANCE_SHEET_FAIL: {
      return {
        ...state,
        balanceSheetLoading: false,
        balanceSheetLoaded: false,
      };
    }
    case fromStockAnalysis.LOAD_STOCK_RATIOS: {
      return {
        ...state,
        ratiosLoading: true,
        ratiosLoaded: false,
      };
    }
    case fromStockAnalysis.LOAD_STOCK_RATIOS_SUCCESS: {
      const ratios: StockRatiosModel[] = action.payload;
      let ticker = '';
      if (ratios.length > 0) {
        ticker = ratios[0].symbol;
      }
      let ratiosEntities = {};
      if (ticker) {
        ratiosEntities = {
          ...state.ratiosEntities,
          [ticker.toUpperCase()]: ratios,
        };
      } else {
        ratiosEntities = {
          ...state.ratiosEntities,
        };
      }

      return {
        ...state,
        ratiosEntities,
        ratiosLoaded: true,
        ratiosLoading: false,
      };
    }
    case fromStockAnalysis.LOAD_STOCK_RATIOS_FAIL: {
      return {
        ...state,
        ratiosLoaded: false,
        ratiosLoading: false,
      };
    }
    default: {
      return state;
    }
  }
}

// Selectors
export const getDcfEntities = (state: StockAnalysisState) => state.dcfEntities;
export const getDcfLoading = (state: StockAnalysisState) => state.dcfLoading;
export const getDcfLoaded = (state: StockAnalysisState) => state.dcfLoaded;
export const getNewsEntities = (state: StockAnalysisState) =>
  state.newsEntities;
export const getNewsLoaded = (state: StockAnalysisState) => state.newsLoaded;
export const getNewsLoading = (state: StockAnalysisState) => state.newsLoading;
export const getBalanceSheetEntities = (state: StockAnalysisState) =>
  state.balanceSheetEntities;
export const getBalanceSheetLoaded = (state: StockAnalysisState) =>
  state.balanceSheetLoaded;
export const getBalanceSheetLoading = (state: StockAnalysisState) =>
  state.balanceSheetLoading;
export const getIncomeStatementEntities = (state: StockAnalysisState) =>
  state.incomeStatementEntities;
export const getIncomeStatementLoading = (state: StockAnalysisState) =>
  state.incomeStatementLoading;
export const getIncomeStatementLoaded = (state: StockAnalysisState) =>
  state.incomeStatementLoaded;
export const getCashFlowEntities = (state: StockAnalysisState) =>
  state.cashFlowEntities;
export const getCashFlowLoading = (state: StockAnalysisState) =>
  state.cashFlowLoading;
export const getCashFlowLoaded = (state: StockAnalysisState) =>
  state.cashFlowLoaded;
export const getRatiosEntities = (state: StockAnalysisState) =>
  state.ratiosEntities;
export const getRatiosLoaded = (state: StockAnalysisState) =>
  state.ratiosLoaded;
export const getRatiosLoading = (state: StockAnalysisState) =>
  state.ratiosLoading;
