import { ActionReducerMap, createFeatureSelector } from '@ngrx/store';

import * as fromStockAnalysis from './stock-analysis.reducer';
import * as fromStockProfile from './stock-profile.reducer';
import * as fromTickerSearch from './ticker-search.reducer';

export interface AppRestrictedState {
  stockAnalysis: fromStockAnalysis.StockAnalysisState;
  stockProfile: fromStockProfile.StockProfileState;
  tickerSearch: fromTickerSearch.TickerSearchState;
}

export const reducers: ActionReducerMap<AppRestrictedState> = {
  stockAnalysis: fromStockAnalysis.reducer,
  stockProfile: fromStockProfile.reducer,
  tickerSearch: fromTickerSearch.reducer,
};

export const getAppRestrictedState = createFeatureSelector<AppRestrictedState>(
  'appRestricted'
);
