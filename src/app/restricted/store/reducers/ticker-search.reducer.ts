import { Ticker } from '../../models/ticker.model';
import * as fromTickerSearchAction from '../actions/ticker-search.action';
import { uniqBy } from 'lodash-es';

export interface TickerSearchState {
  tickersList: Ticker[];
  loading: boolean;
  loaded: boolean;
}

export const initialState: TickerSearchState = {
  tickersList: [],
  loaded: false,
  loading: false,
};

export function reducer(
  state = initialState,
  action: fromTickerSearchAction.tickerSearchAction
): TickerSearchState {
  switch (action.type) {
    case fromTickerSearchAction.SEARCH_TICKERS: {
      return {
        ...state,
        loading: true,
      };
    }
    case fromTickerSearchAction.SEARCH_TICKERS_SUCCESS: {
      const tickers: Ticker[] = action.payload;
      return {
        ...state,
        loading: false,
        loaded: true,
        tickersList: uniqBy([...state.tickersList, ...tickers], 'name'),
      };
    }
    case fromTickerSearchAction.SEARCH_TICKERS_FAIL: {
      return {
        ...state,
        loaded: false,
        loading: false,
      };
    }
    default: {
      return state;
    }
  }
}

// Ticker Search selector
export const getTickersList = (state: TickerSearchState) => state.tickersList;
export const getTickersListLoading = (state: TickerSearchState) =>
  state.loading;
export const getTickersListLoaded = (state: TickerSearchState) => state.loaded;
