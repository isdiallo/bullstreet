import { StockProfile } from '../../models/profile.model';
import * as fromStockProfileAction from '../actions/stock-profile.action';

export interface StockProfileState {
  entities: { [id: string]: StockProfile };
  stockProfileLoading: boolean;
  stockProfileLoaded: boolean;
}
export const initialState: StockProfileState = {
  entities: {},
  stockProfileLoading: false,
  stockProfileLoaded: false,
};

export function reducer(
  state = initialState,
  action: fromStockProfileAction.stockProfileAction
): StockProfileState {
  switch (action.type) {
    case fromStockProfileAction.LOAD_STOCK_PROFILE: {
      return {
        ...state,
        stockProfileLoading: true,
        stockProfileLoaded: false,
      };
    }
    case fromStockProfileAction.LOAD_STOCK_PROFILE_SUCCESS: {
      const profile = action.payload[0];
      let entities = {};
      if (profile) {
        entities = {
          ...state.entities,
          [profile.symbol.toUpperCase()]: profile,
        };
      } else {
        entities = {
          ...state.entities,
        };
      }

      return {
        ...state,
        stockProfileLoaded: true,
        stockProfileLoading: false,
        entities,
      };
    }
    case fromStockProfileAction.LOAD_STOCK_PROFILE_FAIL: {
      return {
        ...state,
        stockProfileLoaded: false,
        stockProfileLoading: false,
      };
    }
    default: {
      return state;
    }
  }
}

// Selectors
export const getEntities = (state: StockProfileState) => state.entities;
export const getStockProfileLoaded = (state: StockProfileState) =>
  state.stockProfileLoaded;
export const getStockProfileLoading = (state: StockProfileState) =>
  state.stockProfileLoading;
