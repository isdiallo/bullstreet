import { createSelector } from '@ngrx/store';
import * as fromFeatures from '../reducers';
import * as fromTickerSearch from '../reducers/ticker-search.reducer';

export const getTickerSearchState = createSelector(
  fromFeatures.getAppRestrictedState,
  (state: fromFeatures.AppRestrictedState) => state.tickerSearch
);

export const getTickersList = createSelector(
  getTickerSearchState,
  fromTickerSearch.getTickersList
);

export const getTickersListLoading = createSelector(
  getTickerSearchState,
  fromTickerSearch.getTickersListLoading
);
export const getTickersListLoaded = createSelector(
  getTickerSearchState,
  fromTickerSearch.getTickersListLoaded
);
