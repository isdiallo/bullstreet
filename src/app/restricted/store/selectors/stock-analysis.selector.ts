import { createSelector } from '@ngrx/store';
import * as fromFeatures from '../reducers';
import * as fromStockAnalysis from '../reducers/stock-analysis.reducer';

export const getStockAnalysisState = createSelector(
  fromFeatures.getAppRestrictedState,
  (state: fromFeatures.AppRestrictedState) => state.stockAnalysis
);
// Stock DCF
export const getDcfEntities = createSelector(
  getStockAnalysisState,
  fromStockAnalysis.getDcfEntities
);
export const getDcfLoaded = createSelector(
  getStockAnalysisState,
  fromStockAnalysis.getDcfLoaded
);
export const getDcfLoading = createSelector(
  getStockAnalysisState,
  fromStockAnalysis.getDcfLoading
);
export const getStockDcf = (ticker: string) =>
  createSelector(getDcfEntities, (entities) => entities[ticker.toUpperCase()]);
// Stock News
export const getNewsEntities = createSelector(
  getStockAnalysisState,
  fromStockAnalysis.getNewsEntities
);
export const getNewsLoading = createSelector(
  getStockAnalysisState,
  fromStockAnalysis.getNewsLoading
);
export const getNewsLoaded = createSelector(
  getStockAnalysisState,
  fromStockAnalysis.getNewsLoaded
);
export const getStockNews = (ticker: string) =>
  createSelector(getNewsEntities, (entities) => entities[ticker.toUpperCase()]);
// Stock Balance Sheet
export const getBalanceSheetEntities = createSelector(
  getStockAnalysisState,
  fromStockAnalysis.getBalanceSheetEntities
);
export const getBalanceSheetLoaded = createSelector(
  getStockAnalysisState,
  fromStockAnalysis.getBalanceSheetLoaded
);
export const getBalanceSheetLoading = createSelector(
  getStockAnalysisState,
  fromStockAnalysis.getBalanceSheetLoading
);
export const getStockBalanceSheet = (ticker: string) =>
  createSelector(
    getBalanceSheetEntities,
    (entities) => entities[ticker.toUpperCase()]
  );
// Stock Income Statement
export const getIncomeStatementEntities = createSelector(
  getStockAnalysisState,
  fromStockAnalysis.getIncomeStatementEntities
);
export const getIncomeStatementLoaded = createSelector(
  getStockAnalysisState,
  fromStockAnalysis.getIncomeStatementLoaded
);
export const getIncomeStatementLoading = createSelector(
  getStockAnalysisState,
  fromStockAnalysis.getIncomeStatementLoading
);
export const getStockIncomeStatement = (ticker: string) =>
  createSelector(
    getIncomeStatementEntities,
    (entities) => entities[ticker.toUpperCase()]
  );

// Stock Cash Flow
export const getCashFlowEntities = createSelector(
  getStockAnalysisState,
  fromStockAnalysis.getCashFlowEntities
);
export const getCashFlowLoaded = createSelector(
  getStockAnalysisState,
  fromStockAnalysis.getCashFlowLoaded
);
export const getCashFlowLoading = createSelector(
  getStockAnalysisState,
  fromStockAnalysis.getCashFlowLoading
);
export const getStockCashFlow = (ticker: string) =>
  createSelector(
    getCashFlowEntities,
    (entities) => entities[ticker.toUpperCase()]
  );
// Stock Ratios
export const getRatiosEntities = createSelector(
  getStockAnalysisState,
  fromStockAnalysis.getRatiosEntities
);
export const getRatiosLoaded = createSelector(
  getStockAnalysisState,
  fromStockAnalysis.getRatiosLoaded
);
export const getRatiosLoading = createSelector(
  getStockAnalysisState,
  fromStockAnalysis.getRatiosLoading
);
export const getStockRatios = (ticker: string) =>
  createSelector(
    getRatiosEntities,
    (entities) => entities[ticker.toUpperCase()]
  );
