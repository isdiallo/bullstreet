import { createSelector } from '@ngrx/store';
import * as fromFeatures from '../reducers';
import * as fromStockProfile from '../../store/reducers/stock-profile.reducer';

export const getStockProfileState = createSelector(
  fromFeatures.getAppRestrictedState,
  (state: fromFeatures.AppRestrictedState) => state.stockProfile
);

export const getStockProfileEntities = createSelector(
  getStockProfileState,
  fromStockProfile.getEntities
);

export const getStockProfileLoaded = createSelector(
  getStockProfileState,
  fromStockProfile.getStockProfileLoaded
);
export const getStockProfileLoading = createSelector(
  getStockProfileState,
  fromStockProfile.getStockProfileLoading
);

export const getStockProfile = (ticker: string) =>
  createSelector(
    getStockProfileEntities,
    (profileEntities) => profileEntities[ticker.toUpperCase()]
  );
