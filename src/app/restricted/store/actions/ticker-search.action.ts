import { Action } from '@ngrx/store';
import { Ticker } from '../../models/ticker.model';
export const SEARCH_TICKERS = '[Bullstreet::Tickers] Search Tickers';
export const SEARCH_TICKERS_SUCCESS =
  '[Bullstreet::Tickers] Search Tickers Success';
export const SEARCH_TICKERS_FAIL = '[Bullstreet::Tickers] Search Tickers Fail';

export class SearchTickers implements Action {
  constructor(public payload: string) {}
  readonly type = SEARCH_TICKERS;
}

export class SearchTickersSuccess implements Action {
  constructor(public payload: Ticker[]) {}
  readonly type = SEARCH_TICKERS_SUCCESS;
}

export class SearchTickersFail implements Action {
  constructor(public payload: any) {}
  readonly type = SEARCH_TICKERS_FAIL;
}

export type tickerSearchAction =
  | SearchTickers
  | SearchTickersSuccess
  | SearchTickersFail;
