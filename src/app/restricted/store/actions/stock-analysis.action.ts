import { Action } from '@ngrx/store';
import { BalanceSheetModel } from '../../models/balance-sheet.model';
import { CashFlowModel } from '../../models/cash-flow.model';
import { DcfModel } from '../../models/dcf.model';
import { IncomeStatementModel } from '../../models/income-statement.model';
import { NewsModel } from '../../models/news.model';
import { StockRatiosModel } from '../../models/stock-ratios.model';

export const LOAD_DCF = '[Bullstreet::Analysis] Load Discounted Cash Flow';
export const LOAD_DCF_SUCCESS =
  '[Bullstreet::Analysis] Load Discounted Cash Flow Success';
export const LOAD_DCF_FAIL =
  '[Bullstreet::Analysis] Load Discounted Cash Flow Fail';

export class LoadDcf implements Action {
  readonly type = LOAD_DCF;
  constructor(public payload: string) {}
}

export class LoadDcfSuccess implements Action {
  readonly type = LOAD_DCF_SUCCESS;
  constructor(public payload: DcfModel[]) {}
}
export class LoadDcfFail implements Action {
  readonly type = LOAD_DCF_FAIL;
  constructor(public payload: any) {}
}

export const LOAD_NEWS = '[Bullstreet::Stock News] Load Stock News';
export const LOAD_NEWS_SUCCESS =
  '[Bullstreet::Stock News] Load Stock News Success';
export const LOAD_NEWS_FAIL = '[Bullstreet::Stock News] Load Stock News Fail';

export class LoadStockNews implements Action {
  readonly type = LOAD_NEWS;
  constructor(public payload: string) {}
}

export class LoadStockNewsSuccess implements Action {
  readonly type = LOAD_NEWS_SUCCESS;
  constructor(public payload: NewsModel[]) {}
}

export class LoadStockNewsFail implements Action {
  readonly type = LOAD_NEWS_FAIL;
  constructor(public payload: any) {}
}

export const LOAD_CASH_FLOW = '[Bullstreet::Cash Flow] Load Cash Flow';
export const LOAD_CASH_FLOW_SUCCESS =
  '[Bullstreet::Cash Flow] Load Cash Flow Success';
export const LOAD_CASH_FLOW_FAIL =
  '[Bullstreet::Cash Flow] Load Cash Flow Fail';

export class LoadCashFlow implements Action {
  readonly type = LOAD_CASH_FLOW;
  constructor(public payload: string) {}
}

export class LoadCashFlowSuccess implements Action {
  readonly type = LOAD_CASH_FLOW_SUCCESS;
  constructor(public payload: CashFlowModel[]) {}
}

export class LoadCashFlowFail implements Action {
  readonly type = LOAD_CASH_FLOW_FAIL;
  constructor(public payload: any) {}
}

export const LOAD_INCOME_STATEMENT =
  '[Bullstreet::Income Statement] Load Income Statement';
export const LOAD_INCOME_STATEMENT_SUCCESS =
  '[Bullstreet::Income Statement] Load Income Statement Success';
export const LOAD_INCOME_STATEMENT_FAIL =
  '[Bullstreet::Income Statement] Load Income Statement Fail';

export class LoadIncomeStatement implements Action {
  readonly type = LOAD_INCOME_STATEMENT;
  constructor(public payload: string) {}
}

export class LoadIncomeStatementSuccess implements Action {
  readonly type = LOAD_INCOME_STATEMENT_SUCCESS;
  constructor(public payload: IncomeStatementModel[]) {}
}

export class LoadIncomeStatementFail implements Action {
  readonly type = LOAD_INCOME_STATEMENT_FAIL;
  constructor(public payload: any) {}
}

export const LOAD_BALANCE_SHEET =
  '[Bullstreet::Balance Sheet] Load Balance Sheet';
export const LOAD_BALANCE_SHEET_SUCCESS =
  '[Bullstreet::Balance Sheet] Load Balance Sheet Success';
export const LOAD_BALANCE_SHEET_FAIL =
  '[Bullstreet::Balance Sheet] Load Balance Sheet Fail';

export class LoadBalanceSheet implements Action {
  readonly type = LOAD_BALANCE_SHEET;
  constructor(public payload: string) {}
}

export class LoadBalanceSheetSuccess implements Action {
  readonly type = LOAD_BALANCE_SHEET_SUCCESS;
  constructor(public payload: BalanceSheetModel[]) {}
}

export class LoadBalanceSheetFail implements Action {
  readonly type = LOAD_BALANCE_SHEET_FAIL;
  constructor(public payload: any) {}
}

export const LOAD_STOCK_RATIOS = '[Bullstreet::Stock Ratios] Load stock ratios';
export const LOAD_STOCK_RATIOS_SUCCESS =
  '[Bullstreet::Stock Ratios] Load stock ratios success';
export const LOAD_STOCK_RATIOS_FAIL =
  '[Bullstreet::Stock Ratios] Load stock ratios fail';

export class LoadStockRatios implements Action {
  readonly type = LOAD_STOCK_RATIOS;
  constructor(public payload: string) {}
}

export class LoadStockRatiosSuccess implements Action {
  readonly type = LOAD_STOCK_RATIOS_SUCCESS;
  constructor(public payload: StockRatiosModel[]) {}
}

export class LoadStockRatiosFail implements Action {
  readonly type = LOAD_STOCK_RATIOS_FAIL;
  constructor(public payload: any) {}
}
export type StockAnalysisAction =
  | LoadDcf
  | LoadDcfSuccess
  | LoadDcfFail
  | LoadStockNews
  | LoadStockNewsSuccess
  | LoadStockNewsFail
  | LoadCashFlow
  | LoadCashFlowSuccess
  | LoadCashFlowFail
  | LoadIncomeStatement
  | LoadIncomeStatementSuccess
  | LoadIncomeStatementFail
  | LoadBalanceSheet
  | LoadBalanceSheetSuccess
  | LoadBalanceSheetFail
  | LoadStockRatios
  | LoadStockRatiosSuccess
  | LoadStockRatiosFail;
