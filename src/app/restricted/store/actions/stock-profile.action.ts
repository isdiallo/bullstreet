import { Action } from '@ngrx/store';
import { StockProfile } from '../../models/profile.model';

export const LOAD_STOCK_PROFILE = '[Bullstreet::Profile] Load Stock profile';
export const LOAD_STOCK_PROFILE_SUCCESS =
  '[Bullstreet::Profile] Load Stock profile Success';
export const LOAD_STOCK_PROFILE_FAIL =
  '[Bullstreet::Profile] Load Stock profile Fail';

export class LoadStockProfile implements Action {
  readonly type = LOAD_STOCK_PROFILE;
  constructor(public payload: string) {}
}

export class LoadStockProfileSuccess implements Action {
  readonly type = LOAD_STOCK_PROFILE_SUCCESS;
  constructor(public payload: StockProfile[]) {}
}

export class LoadStockProfileFail implements Action {
  readonly type = LOAD_STOCK_PROFILE_FAIL;
  constructor(public payload: any) {}
}

export type stockProfileAction =
  | LoadStockProfile
  | LoadStockProfileSuccess
  | LoadStockProfileFail;
