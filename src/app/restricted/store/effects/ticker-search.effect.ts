import { Injectable } from '@angular/core';
import { TickerSearchService } from '../../services/ticker-search.service';
import { Actions, Effect, ofType } from '@ngrx/effects';
import * as fromTickerSearchAction from '../actions/ticker-search.action';
import { catchError, map, switchMap } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable()
export class TickerSearchEffect {
  constructor(
    private tickerSearchService: TickerSearchService,
    private actions$: Actions
  ) {}

  @Effect()
  getTickers$ = this.actions$.pipe(
    ofType(fromTickerSearchAction.SEARCH_TICKERS),
    map((action: fromTickerSearchAction.SearchTickers) => action.payload),
    switchMap((search: string) =>
      this.tickerSearchService.getTickers(search).pipe(
        map(
          (tickers) => new fromTickerSearchAction.SearchTickersSuccess(tickers)
        ),
        catchError((err) =>
          of(new fromTickerSearchAction.SearchTickersFail(err))
        )
      )
    )
  );
}
