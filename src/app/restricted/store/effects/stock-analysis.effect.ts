import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import * as fromStockAnalysis from '../actions/stock-analysis.action';
import { catchError, map, switchMap } from 'rxjs/operators';
import { of } from 'rxjs';
import { StockAnalysisService } from '../../services/stock-analysis.service';

@Injectable()
export class StockAnalysisEffect {
  constructor(
    private actions$: Actions,
    private stockAnalysisService: StockAnalysisService
  ) {}

  @Effect()
  loadDcf$ = this.actions$.pipe(
    ofType(fromStockAnalysis.LOAD_DCF),
    map((action: fromStockAnalysis.LoadDcf) => action.payload),
    switchMap((ticker) => {
      return this.stockAnalysisService.loadDcf(ticker).pipe(
        map((dcf) => new fromStockAnalysis.LoadDcfSuccess(dcf)),
        catchError((err) => of(new fromStockAnalysis.LoadDcfFail(err)))
      );
    })
  );

  @Effect()
  loadNews$ = this.actions$.pipe(
    ofType(fromStockAnalysis.LOAD_NEWS),
    map((action: fromStockAnalysis.LoadStockNews) => action.payload),
    switchMap((ticker) => {
      return this.stockAnalysisService.loadNews(ticker).pipe(
        map((news) => new fromStockAnalysis.LoadStockNewsSuccess(news)),
        catchError((err) => of(new fromStockAnalysis.LoadStockNewsFail(err)))
      );
    })
  );

  @Effect()
  loadBalanceSheet$ = this.actions$.pipe(
    ofType(fromStockAnalysis.LOAD_BALANCE_SHEET),
    map((action: fromStockAnalysis.LoadBalanceSheet) => action.payload),
    switchMap((ticker) => {
      return this.stockAnalysisService.loadBalanceSheet(ticker).pipe(
        map((bs) => new fromStockAnalysis.LoadBalanceSheetSuccess(bs)),
        catchError((err) => of(new fromStockAnalysis.LoadBalanceSheetFail(err)))
      );
    })
  );

  @Effect()
  loadIncomeStatement$ = this.actions$.pipe(
    ofType(fromStockAnalysis.LOAD_INCOME_STATEMENT),
    map((action: fromStockAnalysis.LoadIncomeStatement) => action.payload),
    switchMap((ticker) => {
      return this.stockAnalysisService.loadIncomeStatement(ticker).pipe(
        map((is) => new fromStockAnalysis.LoadIncomeStatementSuccess(is)),
        catchError((err) =>
          of(new fromStockAnalysis.LoadIncomeStatementFail(err))
        )
      );
    })
  );

  @Effect()
  loadCashFlow$ = this.actions$.pipe(
    ofType(fromStockAnalysis.LOAD_CASH_FLOW),
    map((action: fromStockAnalysis.LoadCashFlow) => action.payload),
    switchMap((ticker) => {
      return this.stockAnalysisService.loadCashFlow(ticker).pipe(
        map((cf) => new fromStockAnalysis.LoadCashFlowSuccess(cf)),
        catchError((err) => of(new fromStockAnalysis.LoadCashFlowFail(err)))
      );
    })
  );

  @Effect()
  loadStockRatios$ = this.actions$.pipe(
    ofType(fromStockAnalysis.LOAD_STOCK_RATIOS),
    map((action: fromStockAnalysis.LoadStockRatios) => action.payload),
    switchMap((ticker) => {
      return this.stockAnalysisService.loadStockRatios(ticker).pipe(
        map((ratios) => new fromStockAnalysis.LoadStockRatiosSuccess(ratios)),
        catchError((err) => of(new fromStockAnalysis.LoadStockRatiosFail(err)))
      );
    })
  );
}
