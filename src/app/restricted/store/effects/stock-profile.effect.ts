import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { StockProfileService } from '../../services/stock-profile.service';
import * as fromStockProfileAction from '../actions/stock-profile.action';
import { catchError, map, switchMap } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable()
export class StockProfileEffect {
  constructor(
    private actions$: Actions,
    private stockProfileService: StockProfileService
  ) {}

  @Effect()
  getStockProfile$ = this.actions$.pipe(
    ofType(fromStockProfileAction.LOAD_STOCK_PROFILE),
    map((action: fromStockProfileAction.LoadStockProfile) => action.payload),
    switchMap((ticker: string) => {
      return this.stockProfileService.getStockProfile(ticker).pipe(
        map(
          (profile) =>
            new fromStockProfileAction.LoadStockProfileSuccess(profile)
        ),
        catchError((err) =>
          of(new fromStockProfileAction.LoadStockProfileFail(err))
        )
      );
    })
  );
}
