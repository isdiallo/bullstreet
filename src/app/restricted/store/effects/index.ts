import { StockAnalysisEffect } from './stock-analysis.effect';
import { StockProfileEffect } from './stock-profile.effect';
import { TickerSearchEffect } from './ticker-search.effect';

export const effects: any[] = [
  StockAnalysisEffect,
  StockProfileEffect,
  TickerSearchEffect,
];

export * from './stock-analysis.effect';
export * from './stock-profile.effect';
export * from './ticker-search.effect';
