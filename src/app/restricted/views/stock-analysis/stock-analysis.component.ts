import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { BalanceSheetModel } from '../../models/balance-sheet.model';
import { CashFlowModel } from '../../models/cash-flow.model';

import { DcfModel } from '../../models/dcf.model';
import { IncomeStatementModel } from '../../models/income-statement.model';
import { NewsModel } from '../../models/news.model';
import { StockProfile } from '../../models/profile.model';
import { StockRatiosModel } from '../../models/stock-ratios.model';
import * as fromRestrictedStore from '../../store';
import * as fromStockProfile from '../../store/actions/stock-profile.action';
@Component({
  selector: 'app-stock-analysis',
  templateUrl: './stock-analysis.component.html',
  styleUrls: ['./stock-analysis.component.scss'],
})
export class StockAnalysisComponent implements OnInit {
  constructor(
    private restrictedStore: Store<fromRestrictedStore.AppRestrictedState>
  ) {}
  profile$: Observable<StockProfile>;
  profileLoading$: Observable<boolean>;
  profileLoaded$: Observable<boolean>;
  dcfLoaded$: Observable<boolean>;
  dcfLoading$: Observable<boolean>;
  dcf$: Observable<DcfModel>;
  news$: Observable<NewsModel[]>;
  newsLoading: Observable<boolean>;
  newsLoaded: Observable<boolean>;
  cashFlow$: Observable<CashFlowModel[]>;
  incomeStatement$: Observable<IncomeStatementModel[]>;
  balanceSheet$: Observable<BalanceSheetModel[]>;
  ratios$: Observable<StockRatiosModel[]>;
  ticker: string;

  searchTicker(ticker: string): void {
    this.ticker = ticker;
    // Load profile
    this.restrictedStore.dispatch(
      new fromStockProfile.LoadStockProfile(ticker)
    );
    // Load DCF
    this.restrictedStore.dispatch(new fromRestrictedStore.LoadDcf(ticker));
    // Load News
    this.restrictedStore.dispatch(
      new fromRestrictedStore.LoadStockNews(ticker)
    );
    // Load income statement
    this.restrictedStore.dispatch(
      new fromRestrictedStore.LoadIncomeStatement(ticker)
    );
    // Load Balance Sheet
    this.restrictedStore.dispatch(
      new fromRestrictedStore.LoadBalanceSheet(ticker)
    );
    // Load Cash Flow
    this.restrictedStore.dispatch(new fromRestrictedStore.LoadCashFlow(ticker));
    // Load Ratios
    this.restrictedStore.dispatch(
      new fromRestrictedStore.LoadStockRatios(ticker)
    );
    // Select from the stores
    this.profile$ = this.restrictedStore.select(
      fromRestrictedStore.getStockProfile(ticker)
    );
    this.dcf$ = this.restrictedStore.select(
      fromRestrictedStore.getStockDcf(ticker)
    );
    this.news$ = this.restrictedStore.select(
      fromRestrictedStore.getStockNews(ticker)
    );
    this.balanceSheet$ = this.restrictedStore.select(
      fromRestrictedStore.getStockBalanceSheet(ticker)
    );
    this.incomeStatement$ = this.restrictedStore.select(
      fromRestrictedStore.getStockIncomeStatement(ticker)
    );
    this.cashFlow$ = this.restrictedStore.select(
      fromRestrictedStore.getStockCashFlow(ticker)
    );
    this.ratios$ = this.restrictedStore.select(
      fromRestrictedStore.getStockRatios(ticker)
    );
  }

  ngOnInit(): void {
    this.profileLoading$ = this.restrictedStore.select(
      fromRestrictedStore.getStockProfileLoading
    );
    this.profileLoaded$ = this.restrictedStore.select(
      fromRestrictedStore.getStockProfileLoaded
    );
  }
}
