import { TickerFilterPipe } from './ticker-filter.pipe';
import { MarketCapPipe } from './market-cap.pipe';

export const pipes: any[] = [TickerFilterPipe, MarketCapPipe];

export * from './ticker-filter.pipe';
export * from './market-cap.pipe';
