import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'valuationPipe',
})
export class ValuationPipePipe implements PipeTransform {
  transform(ratio: number): string {
    if (ratio > 0 && ratio < 0.05) {
      return 'Slightly below';
    } else if (ratio >= 0.05) {
      return 'Below';
    } else if (ratio < 0 && ratio > -0.05) {
      return 'Slightly above';
    } else if (ratio <= -0.05) {
      return 'Above';
    } else {
      return 'Equal';
    }
  }
}
