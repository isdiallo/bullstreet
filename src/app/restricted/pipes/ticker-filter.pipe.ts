import { Pipe, PipeTransform } from '@angular/core';
import { Ticker } from '../models/ticker.model';

@Pipe({
  name: 'tickerFilter',
})
export class TickerFilterPipe implements PipeTransform {
  transform(tickersList: Ticker[], search?: string): Ticker[] {
    if (search) {
      return tickersList.filter(
        (ticker) => ticker.symbol.indexOf(search.toUpperCase()) === 0
      );
    }
    return tickersList.slice();
  }
}
