import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'marketCap',
})
export class MarketCapPipe implements PipeTransform {
  transform(value: number): string {
    if (value / 1000000000000 > 1) {
      return (value / 1000000000000).toFixed(2) + ' T';
    } else if (value / 1000000000 > 1) {
      return (value / 1000000000).toFixed(2) + ' B';
    }
    return (value / 1000000).toFixed(2) + ' M';
  }
}
