import { DcfModel } from './dcf.model';

export interface StockAnalysisModel {
  dcf: DcfModel;
}
