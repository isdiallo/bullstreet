export interface DcfModel {
  symbol: string;
  date: string;
  dcf: number;
  'Stock Price': number;
}
