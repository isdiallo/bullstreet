import { Injectable } from '@angular/core';
import { CashFlowModel } from '../models/cash-flow.model';
import { sortBy, forEach } from 'lodash-es';
import { BalanceSheetModel } from '../models/balance-sheet.model';
import { IncomeStatementModel } from '../models/income-statement.model';
import { StockRatiosModel } from '../models/stock-ratios.model';

@Injectable()
export class TimeSeriesService {
  computeSeries(
    model:
      | CashFlowModel[]
      | BalanceSheetModel[]
      | IncomeStatementModel[]
      | StockRatiosModel[]
  ): any {
    const obj = {};
    const models = sortBy(model, 'date');
    forEach(models, (mod) => {
      Object.keys(mod).forEach((k) => {
        (obj[k] || (obj[k] = [])).push(
          k === 'date' ? mod[k].split('-')[0] : mod[k]
        );
      });
    });
    return obj;
  }
}
