import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { StockProfile } from '../models/profile.model';
import { baseUrl, fpmKey } from './constants';

@Injectable()
export class StockProfileService {
  constructor(private http: HttpClient) {}

  getStockProfile(ticker: string): Observable<StockProfile[]> {
    return this.http
      .get<StockProfile[]>(
        `${baseUrl}profile/${ticker.toUpperCase()}?apikey=${fpmKey}`
      )
      .pipe(catchError((err: any) => throwError(err.json())));
  }
}
