import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Ticker } from '../models/ticker.model';
import { baseUrl, fpmKey } from './constants';

@Injectable()
export class TickerSearchService {
  constructor(private http: HttpClient) {}

  getTickers(search: string): Observable<Ticker[]> {
    return this.http
      .get<Ticker[]>(
        `${baseUrl}search-ticker?query=${search}&limit=15&apikey=${fpmKey}`
      )
      .pipe(catchError((err) => throwError(err.json)));
  }
}
