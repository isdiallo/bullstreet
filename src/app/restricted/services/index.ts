import { from } from 'rxjs';
import { StockAnalysisService } from './stock-analysis.service';
import { StockProfileService } from './stock-profile.service';
import { TickerSearchService } from './ticker-search.service';
import { TimeSeriesService } from './time-series.service';

export const services: any[] = [
  StockAnalysisService,
  StockProfileService,
  TickerSearchService,
  TimeSeriesService,
];

export * from './stock-analysis.service';
export * from './stock-profile.service';
export * from './ticker-search.service';
export * from './time-series.service';
