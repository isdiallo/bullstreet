import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { DcfModel } from '../models/dcf.model';
import { baseUrl, fpmKey } from './constants';
import { catchError } from 'rxjs/operators';
import { NewsModel } from '../models/news.model';
import { IncomeStatementModel } from '../models/income-statement.model';
import { BalanceSheetModel } from '../models/balance-sheet.model';
import { CashFlowModel } from '../models/cash-flow.model';
import { StockRatiosModel } from '../models/stock-ratios.model';

@Injectable()
export class StockAnalysisService {
  constructor(private http: HttpClient) {}
  loadDcf(ticker: string): Observable<DcfModel[]> {
    return this.http
      .get<DcfModel[]>(
        `${baseUrl}discounted-cash-flow/${ticker.toUpperCase()}?apikey=${fpmKey}`
      )
      .pipe(catchError((err: any) => throwError(err.json())));
  }

  loadNews(ticker: string): Observable<NewsModel[]> {
    return this.http
      .get<NewsModel[]>(
        `${baseUrl}stock_news?tickers=${ticker.toUpperCase()}&limit=5&apikey=${fpmKey}`
      )
      .pipe(catchError((err) => throwError(err.json())));
  }

  loadIncomeStatement(ticker: string): Observable<IncomeStatementModel[]> {
    return this.http
      .get<IncomeStatementModel[]>(
        `${baseUrl}income-statement/${ticker.toUpperCase()}?limit=5&apikey=${fpmKey}`
      )
      .pipe(catchError((err) => throwError(err.json())));
  }

  loadBalanceSheet(ticker: string): Observable<BalanceSheetModel[]> {
    return this.http
      .get<BalanceSheetModel[]>(
        `${baseUrl}balance-sheet-statement/${ticker.toUpperCase()}?limit=5&apikey=${fpmKey}`
      )
      .pipe(catchError((err) => throwError(err.json)));
  }

  loadCashFlow(ticker: string): Observable<CashFlowModel[]> {
    return this.http
      .get<CashFlowModel[]>(
        `${baseUrl}cash-flow-statement/${ticker.toUpperCase()}?limit=5&apikey=${fpmKey}`
      )
      .pipe(catchError((err) => throwError(err.json())));
  }

  loadStockRatios(ticker: string): Observable<StockRatiosModel[]> {
    return this.http
      .get<StockRatiosModel[]>(
        `${baseUrl}ratios/${ticker.toUpperCase()}?limit=5&apikey=${fpmKey}`
      )
      .pipe(catchError((err) => throwError(err.json())));
  }
}
