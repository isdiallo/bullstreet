import { NavigationExtras } from '@angular/router';
import { Action } from '@ngrx/store';

export const GO = '[Global Store::Router] GO';
export const BACK = '[Global Store::Router] BACK';
export const FORWARD = '[Global Store::Router] FORWARD';

export class Go implements Action {
  readonly type = GO;
  constructor(
    public payload: {
      path: any[];
      query?: object;
      extras?: NavigationExtras;
    }
  ) {}
}

export class Back implements Action {
  readonly type = BACK;
}

export class Forward implements Action {
  readonly type = FORWARD;
}

export type RouterActions = Go | Forward | Back;
