"use strict";

const express = require("express");
const path = require("path");

const app = express();

app.use(express.static(path.join(__dirname, "dist/bullStreet")));

app.get("*", (req, res) => {
  res.sendFile(path.join(__dirname, "dist/bullStreet/index.html"));
});

app.listen(process.env.PORT || 8080);
